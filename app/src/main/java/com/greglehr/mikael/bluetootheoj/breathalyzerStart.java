package com.greglehr.mikael.bluetootheoj;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothAdapter.LeScanCallback;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Intent;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class breathalyzerStart extends AppCompatActivity {

    Button btnDebug;
    Button btnGo;
    Button btnBatt;
    Button btnSend;

    //ble UUIDS
    //UAT service and associated characteristics.
    public static UUID UART_UUID = UUID.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
    public static UUID TX_UUID = UUID.fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
    public static UUID RX_UUID = UUID.fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");
    //BLE client characteristic for notifications.
    public static UUID CLIENT_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    private BluetoothGatt gatt;
    private BluetoothGattCharacteristic tx;
    private BluetoothGattCharacteristic rx;

    private BluetoothAdapter myBlueTooth = null;

    private BluetoothGattCallback callback = new BluetoothGattCallback() {


        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);
            if (newState == BluetoothGatt.STATE_CONNECTED) {
                if (!gatt.discoverServices()) {
                    Log.v("NEWGATTSTATE", "DISCOVERY FAILED");
                   // Toast.makeText(breathalyzerStart.this, "Discovery Failed", Toast.LENGTH_SHORT).show();
                }
            } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                //Toast.makeText(breathalyzerStart.this, "Disconnected", Toast.LENGTH_SHORT).show();
                Log.v("NEWGATTSTATE", "Disconnected");
            } else {
                Log.v("NEWGATTSTATE", "STATE " + newState);
                //Toast.makeText(breathalyzerStart.this, "State = " + newState, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.v("CONNECTED!!!", "Connection established.  Gatt success!");

                //get rid of our "Not connected" warning
                runOnUiThread(new Runnable(){
                    @Override
                    public void run(){
                        findViewById(R.id.bluetoothPlease).setVisibility(View.GONE);
                    }});
                Log.v("MESSAGE","Cleared nonrunning message");
            } else {
                Toast.makeText(breathalyzerStart.this, "service failure " + status, Toast.LENGTH_SHORT).show();
            }
            tx = gatt.getService(UART_UUID).getCharacteristic(TX_UUID);
            rx = gatt.getService(UART_UUID).getCharacteristic(RX_UUID);
            if (!gatt.setCharacteristicNotification(rx, true)) {
                Toast.makeText(breathalyzerStart.this, "Couldn't set notifications for RX characteristic!", Toast.LENGTH_SHORT).show();
            }
            // Next update the RX characteristic's client descriptor to enable notifications.
            if (rx.getDescriptor(CLIENT_UUID) != null) {
                BluetoothGattDescriptor desc = rx.getDescriptor(CLIENT_UUID);
                desc.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                if (!gatt.writeDescriptor(desc)) {
                    Toast.makeText(breathalyzerStart.this, "Couldn't write RX client descriptor value!", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(breathalyzerStart.this, "Couldn't get RX client descriptor!", Toast.LENGTH_SHORT).show();
            }
            updateBattery();
        }
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status){
            super.onCharacteristicRead(gatt, characteristic, status);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status){
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        // Called when a remote characteristic changes (like the RX characteristic).
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            //writeLine("Received: " + characteristic.getStringValue(0));
            final String returnedResult = characteristic.getStringValue(0);
            //For some reason, updating the UI isn't working here without forcing it to main thread.
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (returnedResult.startsWith("b")) {
                        TextView d = (TextView) findViewById(R.id.resultDisplay);
                        d.setText(returnedResult.substring(1));
                        findViewById(R.id.progSpinner).setVisibility(View.GONE);
                        findViewById(R.id.resultDisplay).setVisibility(View.VISIBLE);
                    } else {
                        TextView d = (TextView) findViewById(R.id.battLevel);
                        d.setText(returnedResult.substring(1));
                        findViewById(R.id.battLevel).setVisibility(View.VISIBLE);
                        findViewById(R.id.batSpinner).setVisibility(View.GONE);
                    }
                }
            });
        }
    };


    // BLE device scanning callback.
    private LeScanCallback scanCallback = new LeScanCallback() {
        // Called when a device is found.
        @Override
        public void onLeScan(BluetoothDevice bluetoothDevice, int i, byte[] bytes) {
            //writeLine("Found device: " + bluetoothDevice.getAddress());
            // Check if the device has the UART service.
            if (parseUUIDs(bytes).contains(UART_UUID)) {
                // Found a device, stop the scan.
                myBlueTooth.stopLeScan(scanCallback);
                //writeLine("Found UART service!");
                // Connect to the device.
                // Control flow will now go to the callback functions when BTLE events occur.
                gatt = bluetoothDevice.connectGatt(getApplicationContext(), false, callback);
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breathalyzer_start);
        findViewById(R.id.resultDisplay).setVisibility(View.INVISIBLE);
        findViewById(R.id.progSpinner).setVisibility(View.GONE);
        findViewById(R.id.battLevel).setVisibility(View.INVISIBLE);

        btnDebug = (Button) findViewById(R.id.ble);
        btnDebug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(breathalyzerStart.this, ble.class));
            }
        });

        btnGo = (Button) findViewById(R.id.button1);
        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.progSpinner).setVisibility(View.VISIBLE);
                findViewById(R.id.resultDisplay).setVisibility(View.INVISIBLE);
                // ((TextView) findViewById(R.id.resultDisplay)).setText("Loading...");
                myBlueTooth = BluetoothAdapter.getDefaultAdapter();
                String message = "GO";
                if (tx == null) {
                    Toast.makeText(breathalyzerStart.this, "No device to send to.", Toast.LENGTH_SHORT).show();
                    return;
                }
                tx.setValue(message.getBytes(Charset.forName("UTF-8")));
                if (!gatt.writeCharacteristic(tx)) {
                    Toast.makeText(breathalyzerStart.this, "Failed to start.", Toast.LENGTH_SHORT).show();
                }

            }
        });


        btnBatt = (Button) findViewById(R.id.batt);
        btnBatt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                findViewById(R.id.battLevel).setVisibility(View.INVISIBLE);
                findViewById(R.id.batSpinner).setVisibility(View.VISIBLE);
                myBlueTooth = BluetoothAdapter.getDefaultAdapter();
                String message = "BA";
                if (tx == null) {
                    Toast.makeText(breathalyzerStart.this, "No device to send to.", Toast.LENGTH_SHORT).show();
                    return;
                }
                tx.setValue(message.getBytes(Charset.forName("UTF-8")));
                if (!gatt.writeCharacteristic(tx)) {
                    Toast.makeText(breathalyzerStart.this, "Failed to start.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnSend = (Button) findViewById(R.id.toResultsSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(breathalyzerStart.this, results.class);
                Bundle testResults = new Bundle();
                String text = ((TextView) findViewById(R.id.resultDisplay)).getText().toString();
                testResults.putString("BAC", text);
                i.putExtras(testResults);
                startActivity(i);
            }
        });


        //Let's make sure Bluetooth is running.
        myBlueTooth = BluetoothAdapter.getDefaultAdapter();
        if (myBlueTooth == null) {
            Toast.makeText(getApplicationContext(), "Bluetooth not supported", Toast.LENGTH_LONG).show();
            finish();
        } else if (!myBlueTooth.isEnabled()) {
            Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnOn, 1);
            findViewById(R.id.bluetoothPlease).setVisibility(View.VISIBLE);
        }
    }


    void updateBattery() {
        Log.v("BATT","Requesting Battery State");
        new Thread(new Runnable(){
            @Override
            public void run()
            {
                try{
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable(){
                    @Override
                public void run()
                    {
                        myBlueTooth = BluetoothAdapter.getDefaultAdapter();
                        String message = "BA";
                        if (tx == null) {
                            Log.v("null", "tx is null");
                            Toast.makeText(breathalyzerStart.this, "Battery: No device to send to.", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        tx.setValue(message.getBytes(Charset.forName("UTF-8")));
                        if (!gatt.writeCharacteristic(tx)) {
                            Log.v("NULLPTR", "failed to write TX. should try again.");
                            //Toast.makeText(breathalyzerStart.this, "Battery: Failed to start.", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }).start();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        myBlueTooth.startLeScan(scanCallback);
    }


    @Override
    protected void onStop()
    {
        super.onStop();
        if (gatt != null)
        {
            gatt.disconnect();
            gatt.close();
            gatt = null;
            tx = null;
            rx = null;
        }

    }





    //   http://stackoverflow.com/questions/18019161/startlescan-with-128-bit-uuids-doesnt-work-on-native-android-ble-implementation?noredirect=1#comment27879874_18019161
        private List<UUID> parseUUIDs(final byte[] advertisedData) {
            List<UUID> uuids = new ArrayList<UUID>();

            int offset = 0;
            while (offset < (advertisedData.length - 2)) {
                int len = advertisedData[offset++];
                if (len == 0)
                    break;

                int type = advertisedData[offset++];
                switch (type) {
                    case 0x02: // Partial list of 16-bit UUIDs
                    case 0x03: // Complete list of 16-bit UUIDs
                        while (len > 1) {
                            int uuid16 = advertisedData[offset++];
                            uuid16 += (advertisedData[offset++] << 8);
                            len -= 2;
                            uuids.add(UUID.fromString(String.format("%08x-0000-1000-8000-00805f9b34fb", uuid16)));
                        }
                        break;
                    case 0x06:// Partial list of 128-bit UUIDs
                    case 0x07:// Complete list of 128-bit UUIDs
                        // Loop through the advertised 128-bit UUID's.
                        while (len >= 16) {
                            try {
                                // Wrap the advertised bits and order them.
                                ByteBuffer buffer = ByteBuffer.wrap(advertisedData, offset++, 16).order(ByteOrder.LITTLE_ENDIAN);
                                long mostSignificantBit = buffer.getLong();
                                long leastSignificantBit = buffer.getLong();
                                uuids.add(new UUID(leastSignificantBit,
                                        mostSignificantBit));
                            } catch (IndexOutOfBoundsException e) {
                                // Defensive programming.
                                //Log.e(LOG_TAG, e.toString());
                                continue;
                            } finally {
                                // Move the offset to read the next uuid.
                                offset += 15;
                                len -= 16;
                            }
                        }
                        break;
                    default:
                        offset += (len - 1);
                        break;
                }
            }
            return uuids;
        }


}
