package com.greglehr.mikael.bluetootheoj;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class offlineMode extends AppCompatActivity {

    RadioGroup sexGroup;
    RadioButton selectedSex;
    Button btnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_mode);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        sexGroup = (RadioGroup) findViewById(R.id.radioSex);
        btnNext = (Button) findViewById(R.id.btnNext);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedSex = (RadioButton) findViewById(sexGroup.getCheckedRadioButtonId());

                if(sexGroup.getCheckedRadioButtonId()==-1)
                {
                    Snackbar.make(v, "Please choose a sex!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                // Make an intent to start next activity.
                else
                {
                    Intent i = new Intent(offlineMode.this, question2.class);
                    //Change the activity.
                    i.putExtra("SEX", selectedSex.getText().toString()); //this will be received at ledControl (class) Activity
                    startActivity(i);
                }

            }
        });



    }

}
