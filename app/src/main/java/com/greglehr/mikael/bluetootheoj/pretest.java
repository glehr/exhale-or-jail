package com.greglehr.mikael.bluetootheoj;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class pretest extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pretest);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button btnYes = (Button)findViewById(R.id.btnYes);
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(pretest.this, breathalyzerStart.class));
            }
        });

        Button btnNo = (Button)findViewById(R.id.btnNo);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, "Please wait at least 15 minutes before testing.", Snackbar.LENGTH_INDEFINITE)
                        .setAction("Go Back", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                startActivity(new Intent(pretest.this, launcher.class));
                            }
                        }).show();
            }
        });
    }

}
