package com.greglehr.mikael.bluetootheoj;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

public class question3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question3);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        Intent intent = getIntent();
        Bundle previous = intent.getExtras();
        final String selectedSex = previous.getString("SEX");
        final String weight = previous.getString("WEIGHT");

        String temp = selectedSex + "," + weight;
        Toast.makeText(getApplicationContext(), temp, Toast.LENGTH_SHORT).show();
        Button btnNext = (Button) findViewById(R.id.btnNext);


        String[] times = new String[33];
        times[0] = "<15 minutes";
        times[1] = "15 minutes";
        times[2] = "30 minutes";
        times[3] = "45 minutes";
        times[4] = "1 hour";
        times[5] = "1 hour 15 minutes";
        times[6] = "1 hour 30 minutes";
        times[7] = "1 hour 45 minutes";
        times[8] = "2 hours";
        times[9] = "2 hours 15 minutes";
        times[10] = "2 hours 30 minutes";
        times[11] = "2 hours 45 minutes";
        times[12] = "3 hours";
        times[13] = "3 hours 15 minutes";
        times[14] = "3 hours 30 minutes";
        times[15] = "3 hours 45 minutes";
        times[16] = "4 hours";
        times[17] = "4 hours 15 minutes";
        times[18] = "4 hours 30 minutes";
        times[19] = "4 hours 45 minutes";
        times[20] = "5 hours";
        times[21] = "5 hours 15 minutes";
        times[22] = "5 hours 30 minutes";
        times[23] = "5 hours 45 minutes";
        times[24] = "6 hours";
        times[25] = "6 hours 15 minutes";
        times[26] = "6 hours 30 minutes";
        times[27] = "6 hours 45 minutes";
        times[28] = "7 hours";
        times[29] = "7 hours 15 minutes";
        times[30] = "7 hours 30 minutes";
        times[31] = "7 hours 45 minutes";
        times[32] = "8 hours or more";

        ArrayAdapter<String> timeArray = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, times);
        spinner.setAdapter(timeArray);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (spinner.getSelectedItemPosition()==0) {
                    Snackbar.make(v, "Please wait 15 minutes before testing!", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
                // Make an intent to start next activity.
                else {
                    String time = Integer.toString(spinner.getSelectedItemPosition());
                    Intent i = new Intent(question3.this, question4.class);
                    Bundle previous = new Bundle();
                    previous.putString("SEX", selectedSex);
                    previous.putString("WEIGHT", weight);
                    previous.putString("TIME", time);
                    i.putExtras(previous);
                    startActivity(i);
                }
            }

        });

    }
}