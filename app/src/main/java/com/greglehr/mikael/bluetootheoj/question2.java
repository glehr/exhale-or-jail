package com.greglehr.mikael.bluetootheoj;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.Iterator;
import java.util.List;


public class question2 extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        final Spinner spinner = (Spinner) findViewById(R.id.spinner);
        setSupportActionBar(toolbar);
        //Read from question 1
        Intent intent = getIntent();
        Bundle previous = intent.getExtras();
        final String selectedSex = previous.getString("SEX");

        Button btnNext = (Button) findViewById(R.id.btnNext);

       // Toast.makeText(getApplicationContext(),selectedSex,Toast.LENGTH_SHORT).show();

       /* Iterable<Integer> values = range(80,400);
        List<Integer> weightList = newLinkedList(values);
        int[] array = toArray(weightList);

        List<Integer> list = newLinkedList();
        for (int i = 80; i <= 400; i++)
        {
            list.add(i);
        }
        */

        String[] weights = new String[351];
        for (int i = 0;i<=350;i++)
        {
            weights[i]=Integer.toString(i+50)+" lbs";
        }

        ArrayAdapter<String> weightArray = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,weights);
        spinner.setAdapter(weightArray);

        spinner.setSelection(200, true);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String weight = Integer.toString(spinner.getSelectedItemPosition()+50);
                //string selectedSex
                Intent i = new Intent(question2.this, question3.class);
                Bundle previous = new Bundle();
                previous.putString("SEX",selectedSex);
                previous.putString("WEIGHT",weight);
                i.putExtras(previous);
                startActivity(i);
                }

            });

    }

}
