package com.greglehr.mikael.bluetootheoj;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class question4 extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question4);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Bundle previous = intent.getExtras();
        final String selectedSex = previous.getString("SEX");
        final String weight = previous.getString("WEIGHT");
        final String times = previous.getString("TIME");

        Button btnNext = (Button) findViewById(R.id.btnNext);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String proof = ((EditText) findViewById(R.id.alcLevel)).getText().toString();
                String volume = ((EditText) findViewById(R.id.volumeDrunk)).getText().toString();

                float floatProof;
                float floatVolume;

                try {
                    floatProof = Float.parseFloat(proof);
                } catch (NumberFormatException nfe) {
                    floatProof = -1;
                }

                try {
                    floatVolume = Float.parseFloat(volume);
                } catch (NumberFormatException nfe) {
                    floatVolume = -1;
                }

                if (floatProof > 100 || floatProof < 0) {
                    Snackbar.make(v, "Is that the correct alcohol concentration?", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (floatVolume > 9999 || floatVolume < 1) {
                    Snackbar.make(v, "Is that really how much you drank?", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

                // Make an intent to start next activity.
                else {
                    Intent i = new Intent(question4.this, results.class);
                    Bundle offlineResults = new Bundle();

                    //Calculating BAC:
                    // http://www.endmemo.com/medical/bac.php
                    // BAC% = (A × 5.14/W × r) - .015 × H
                    // Where:
                    // A: Total alcohol consumed, in ounces (oz)
                    // W: Body weight, in pounds (lbs)
                    // r: The alcohol distribution ratio, 0.73 for man, and 0.66 for women
                    // H: Time passed since drinking, in hours

                    double sexConst = (selectedSex.equals("Male")) ? .73 : .66;
                    double hoursDrinking = Integer.parseInt(times)*.25;
                    double alcohol = floatProof * floatVolume/100;
                    int iWeight = Integer.parseInt(weight);
                    double first = alcohol*5.14/iWeight * sexConst;
                    double last = .015*hoursDrinking;
                    double BAC = (first - last);

                    String formattedBAC = String.format("%.3f", BAC);
                    offlineResults.putString("BAC", formattedBAC);

                    //EVENTUALLY DELETE THESE:
                   /* offlineResults.putString("dSex", Double.toString(sexConst));
                    offlineResults.putString("dHours", Double.toString(hoursDrinking));
                    offlineResults.putString("dAlcohol", Double.toString(alcohol));
                    offlineResults.putString("dFirst", Double.toString(first));
                    offlineResults.putString("dLast", Double.toString(last));
                    offlineResults.putString("unfBAC", Double.toString(BAC));
                    offlineResults.putString("dWeight", weight);
*/
                    i.putExtras(offlineResults);
                    startActivity(i);
                }
            }

        });


        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
