package com.greglehr.mikael.bluetootheoj;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Random;


public class results extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        Bundle previous = intent.getExtras();

        //eventually delete these:
        /*
        final String sex = previous.getString("dSex");
        final String hours = previous.getString("dHours");
        final String alcohol = previous.getString("dAlcohol");
        final String weight = previous.getString("dWeight");
        final String first = previous.getString("dFirst");
        final String last = previous.getString("dLast");
        final String unfBAC = previous.getString("unfBAC");
        String hmm = "sex: "+sex+" \nhours: "+hours+" \nalcohol: "+alcohol+" \nweight: "+weight+"\nfirst: "+first+"\nLast: "+last + "\nBAC: "+unfBAC;
        TextView d = (TextView)findViewById(R.id.debug);
        d.setText(hmm);  */

        Button startover = (Button)findViewById(R.id.btnStartOver);
        startover.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(results.this, launcher.class));
            }
        });

        if (previous != null)
            {
                if (previous.containsKey("BAC"))
                {
                    final String BAC = previous.getString("BAC");
                    TextView t = (TextView)findViewById(R.id.bacResults);
                    t.setText(BAC);
                }
            }
        }

}
