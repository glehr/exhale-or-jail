/*********************************************************************
  This is an example for our nRF51822 based Bluefruit LE modules

  Pick one up today in the adafruit shop!

  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  MIT license, check LICENSE for more information
  All text above, and the splash screen below must be included in
  any redistribution
*********************************************************************/

#include <Arduino.h>
#include <SPI.h>
#if not defined (_VARIANT_ARDUINO_DUE_X_) && not defined (_VARIANT_ARDUINO_ZERO_)
#include <SoftwareSerial.h>
#endif

#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BluefruitLE_UART.h"

#include "BluefruitConfig.h"

/*=========================================================================
    APPLICATION SETTINGS

      FACTORYRESET_ENABLE       Perform a factory reset when running this sketch
     
                                Enabling this will put your Bluefruit LE module
                              in a 'known good' state and clear any config
                              data set in previous sketches or projects, so
                                running this at least once is a good idea.
     
                                When deploying your project, however, you will
                              want to disable factory reset by setting this
                              value to 0.  If you are making changes to your
                                Bluefruit LE device via AT commands, and those
                              changes aren't persisting across resets, this
                              is the reason why.  Factory reset will erase
                              the non-volatile memory where config data is
                              stored, setting it back to factory default
                              values.
         
                                Some sketches that require you to bond to a
                              central device (HID mouse, keyboard, etc.)
                              won't work at all with this feature enabled
                              since the factory reset will clear all of the
                              bonding data stored on the chip, meaning the
                              central device won't be able to reconnect.
    MINIMUM_FIRMWARE_VERSION  Minimum firmware version to have some new features
    MODE_LED_BEHAVIOUR        LED activity, valid options are
                              "DISABLE" or "MODE" or "BLEUART" or
                              "HWUART"  or "SPI"  or "MANUAL"
    -----------------------------------------------------------------------*/
#define FACTORYRESET_ENABLE         0
#define MINIMUM_FIRMWARE_VERSION    "0.6.6"
#define MODE_LED_BEHAVIOUR          "MODE"
/*=========================================================================*/

Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);

// A small helper
void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}

/**************************************************************************/
/*!
    @brief  Sets up the HW an the BLE module (this function is called
            automatically on startup)
*/
/**************************************************************************/
void setup(void)
{
  //while (!Serial);  // required for Flora & Micro
  //delay(500);

  Serial.begin(115200);
  Serial.println(F("Adafruit Bluefruit Command Mode Example"));
  Serial.println(F("---------------------------------------"));

  /* Initialise the module */
  Serial.print(F("Initialising the Bluefruit LE module: "));

  if ( !ble.begin(VERBOSE_MODE) )
  {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  Serial.println( F("OK!") );

  /* Disable command echo from Bluefruit */
  ble.echo(false);

  Serial.println("Requesting Bluefruit info:");
  /* Print Bluefruit information */
  ble.info();




  ble.verbose(false);  // debug info is a little annoying after this point!

  /* Wait for connection */
  while (! ble.isConnected()) {
    delay(500);
  }

  //WAIT FOR REQUEST TO MEASURE:



  // LED Activity command is only supported from 0.6.6
  if ( ble.isVersionAtLeast(MINIMUM_FIRMWARE_VERSION) )
  {
    // Change Mode LED Activity
    Serial.println(F("******************************"));
    Serial.println(F("Change LED activity to " MODE_LED_BEHAVIOUR));
    ble.sendCommandCheckOK("AT+HWModeLED=" MODE_LED_BEHAVIOUR);

    // ble.sendATcommand();
    Serial.println(F("******************************"));
  }

}

int getBatteryLife(void)
{
  float measuredvbat = analogRead(A9);
  measuredvbat *= 2;    // we divided by 2, so multiply back
  measuredvbat *= 3.3;  // Multiply by 3.3V, our reference voltage
  measuredvbat /= 1024; // convert to voltage
  Serial.print("VBat: " ); Serial.println(measuredvbat);
  //measuredvbat = (measuredvbat - 3.21) * 100;
  //We're no longer using a linear formula for battery percentage.
  //Instead, voltages have been divided into 10 categories.
  //This is based on the properties of LiPoly batteries,
  //rather than our previous linear model assuming the battery will evenly
  //discharge from 4.2V to 3.2V.
  
  int category = 0;
  if (measuredvbat >= 4.18)
    category = 9;
  else if (measuredvbat >= 4.03)
    category = 8;
  else if (measuredvbat >= 4.03)
    category = 7;
  else if (measuredvbat >= 3.86)
    category = 6;
  else if (measuredvbat >= 3.83)
    category = 5;
  else if (measuredvbat >= 3.79)
    category = 4;   
  else if (measuredvbat >= 3.70)
    category = 3;   
  else if (measuredvbat >= 3.60)
    category = 2;   
  else if (measuredvbat >= 3.4)
    category = 1;   
  else
    category = 0;
  
  return category;
}
String AlcTest(void)
{
  pinMode(A0, INPUT);
  pinMode(13, OUTPUT);
  float maxV = 0.00;

  for (int i = 0; i < 10; i++)
  {
    digitalWrite(13, HIGH);
    float val = analogRead(A0) * (3300 / 1023.0);
    Serial.println(val);
    if (val > maxV)
    {
      maxV = val;
      Serial.print(maxV); Serial.println(F("<--MAX"));
    }
    delay(200);
    digitalWrite(13, LOW);
    delay(200);
  }
  return String(maxV, 3);
}

void loop(void)
{
  // Check for user input
  //  char inputs[BUFSIZE+1];

  // Check for incoming characters from Bluefruit
  ble.println("AT+BLEUARTRX");
  ble.readline();
  if (strcmp(ble.buffer, "OK") == 0) {
    // no data
    return;
  }
  // Some data was found, its in the buffer
  String test = ble.buffer;

  if (test == "GO")
  {
    Serial.println(F("READY TO GO!"));
    String result = AlcTest();
    ble.print("AT+BLEUARTTX=b"); ble.println(result);

    //did it send?
    if (! ble.waitForOK() )
    {
      Serial.println(F("Failed to send?"));
    }
  }
  else if (test == "BA")
  {
    Serial.println(F("Battery info requested"));
    ble.print("AT+BLEUARTTX=e"); ble.println(getBatteryLife());
    //did it send?
    if (! ble.waitForOK() )
    {
      Serial.println(F("Failed to send?"));
    }
  }
  else
  {
    Serial.print(F("Not recognized: ")); Serial.println(test);
  }

  ble.waitForOK();
}

